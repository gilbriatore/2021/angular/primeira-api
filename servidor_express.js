const express = require('express');
const app = express();

const quadrado = require('./quadrado');
const calculadora = require('./calculadora');

app.get('/', function(req, res){
  res.send(`A área do quadrado de 4 é: ${quadrado.area(4)}<br>
            A soma de 2 + 2 é: ${calculadora.somar(2, 2)}
           `);
});


app.listen(3000, function(){
  console.log('App rodando na porta 3000!');
});