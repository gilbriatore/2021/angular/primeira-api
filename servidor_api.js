const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
app.use(morgan('dev'));
app.use(express.urlencoded({extended: true}))
app.use(express.json());
app.use(cors());

//npm install cors
//var cors = require('cors');
//app.use(cors());

app.get('/', function(req, res) {
  res.send('GET');
});

app.post('/api', function(req, res) {
  res.json({text: req.body.texto});
});

const produtos = require('./produtos.json');

app.get('/produtos', function(req, res) {
  res.json(produtos);
});

app.listen(3000, function() {
  console.log('App rodando na porta 3000!'); 
}); 




